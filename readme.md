Shaggy's Configs
================

Let me make a few things clear, Iam NOT some pro coder, But this is how I've found out how to do what I need.
* I'm sharing with hopes someone will find things useful
* I dont care what you use anything for.
* I'd love to help you figure out anything if I have the time. Send me an email to twodopeshaggy@gmail.com with the subject related to the question.
* This readme if far from complete.


Setting up WMFS
---------------

![shaggyconfigs](https://raw.github.com/shaggytwodope/shaggyconfigs/master/screens/wmfs.png)


Well first off, I run a script located in ~/.config/wmfs/scripts called echeck.sh You'll need to enter your login info there.
And make a cronjob like below to create a file called .emailstatus in your home dir. Editing the user name shaggy to your own.
````
*/3 * * * *  /home/shaggy/.config/wmfs/scripts/echeck.sh > /home/shaggy/.emailstatus
```
Now for rss, I use newsbeuter to reload the feeds and then tell me if theres new feeds or not.
```
*/30 * * * * /usr/local/bin/newsbeuter -x reload && /usr/local/bin/newsbeuter -x print-unread > /home/shaggy/.rsscount
```
You will need to enter the follow two lines to your .xinitrc file to use the statusbar script as well as starting wmfs, if you already start wmfs properly ignore that line.
```
sh .config/wmfs/caritasstatus.sh &

exec dbus-launch wmfs
```

The battery function for the statusbar is not enabled, you will need to add ``` $(battery)\ ```
